#include <stdio.h>
#include <stdlib.h>
#include "hash.h"

//lex.yy.h
int yylex();
int yyparse();

extern char *yytext;
extern FILE *yyin;

int isRunning(void);
int getLineNumber(void);
void initMe(void);
void printHash(void);

int main(int argc, char **argv) 
{
	if(argc != 2)
	{
		fprintf(stdout, "Numero de parametros invalido!\n");
		exit(1);
	}

	if ((yyin = fopen(argv[1], "r")) == 0)
	{
		fprintf(stdout, "Nao foi possivel abrir o arquivo!\n");
		exit(2);
	}

	while (isRunning())
	{
		yyparse();
	}

	printHash();

	fclose(yyin);

	exit(0);
}
