#define HASH_SIZE 997

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct hashnode{
    int type;
    char* text;
    struct hashnode* next;
}HASH_NODE;

void hashInit();
int hashAddress(char *text);
HASH_NODE* hashFindElseInsert(int type, char *text);
HASH_NODE* hashInsert(int type, char *text);
HASH_NODE* hashFind(char *text);
void hashPrint(void);
