
%{

#include <stdio.h>
#include <stdlib.h>

int getLineNumber();


%}

%union {
	int value;
}

%token KW_CHAR
%token KW_INT
%token KW_FLOAT
%token KW_IF
%token KW_THEN
%token KW_ELSE
%token KW_WHILE
%token KW_READ
%token KW_RETURN
%token KW_PRINT

%token<value> LIT_CHAR
%token<value> LIT_INTEGER
%token<value> LIT_FLOAT
%token<value> LIT_STRING

%token CH_COMMA
%token CH_OPENCMD
%token CH_CLOSECMD
%token CH_OPENITER
%token CH_CLOSEITER
%token CH_OPENBLOCK
%token CH_CLOSEBLOCK
%token CH_EOI
%token CH_ARRAYATTRIB

%token OPERATOR_ATTRIB
%token OPERATOR_GT
%token OPERATOR_LT
%token OPERATOR_PLUS
%token OPERATOR_MINUS
%token OPERATOR_MUL
%token OPERATOR_DIV

%token OPERATOR_EQ
%token OPERATOR_GE
%token OPERATOR_LE
%token OPERATOR_AND
%token OPERATOR_OR
%token OPERATOR_NOT 

%token TK_IDENTIFIER
%token TOKEN_ERROR

%left OPERATOR_ATTRIB
%left OPERATOR_AND OPERATOR_OR
%left OPERATOR_EQ OPERATOR_LE OPERATOR_GE OPERATOR_LT OPERATOR_GT
%left OPERATOR_PLUS OPERATOR_MINUS
%left OPERATOR_MUL OPERATOR_DIV

%left CH_COMMA
%left CH_CLOSECMD
%left TK_IDENTIFIER

%left KW_ELSE
%left KW_THEN

%left CH_EOI

%%

program : cmdlist
	;

cmdlist : cmd CH_EOI cmdlist
	| func cmdlist
	|
	;

cmdblock 	: CH_OPENBLOCK cmdlist CH_CLOSEBLOCK
		;

cmd	: TK_IDENTIFIER OPERATOR_ATTRIB expr
	| TK_IDENTIFIER CH_OPENITER expr CH_CLOSEITER OPERATOR_ATTRIB expr
	| type identifierseq
	| type identifierseq OPERATOR_ATTRIB expr
	| type TK_IDENTIFIER CH_OPENITER LIT_INT CH_CLOSEITER CH_ARRAYATTRIB arrayelements
	| KW_PRINT CH_OPENCMD expr CH_CLOSECMD
	| KW_READ CH_OPENCMD TK_IDENTIFIER CH_CLOSECMD
	| KW_RETURN expr
	| KW_IF expr KW_THEN cmdblock
	| KW_IF expr KW_THEN cmd
	| KW_IF expr KW_THEN cmdblock KW_ELSE cmdblock
	| KW_IF expr KW_THEN cmd KW_ELSE cmdblock
	| KW_IF expr KW_THEN cmdblock KW_ELSE cmd
	| KW_IF expr KW_THEN cmd KW_ELSE cmd
	
	
	| KW_WHILE expr cmdblock
	| KW_WHILE expr cmd
	|
	;

identifierseq	: TK_IDENTIFIER CH_COMMA identifierseq
		| TK_IDENTIFIER
		;

func	: type TK_IDENTIFIER CH_OPENCMD funcargs CH_CLOSECMD cmdblock
	| type TK_IDENTIFIER CH_OPENCMD CH_CLOSECMD cmdblock
	;

funcargs	: type TK_IDENTIFIER CH_COMMA funcargs
		| type TK_IDENTIFIER
		;

type	: KW_INT
	| KW_CHAR
	| KW_FLOAT
	;

expr	: LIT_INTEGER		{ fprintf(stdout, "Found LIT_INT %d\n", $1); }
	| LIT_CHAR
	| LIT_FLOAT
	| LIT_STRING
	| CH_OPENCMD expr CH_CLOSECMD
	| expr OPERATOR_PLUS expr
	| expr OPERATOR_MINUS expr
	| expr OPERATOR_MUL expr
	| expr OPERATOR_DIV expr
	| expr OPERATOR_GT expr
	| expr OPERATOR_LT expr
	| expr OPERATOR_GE expr
	| expr OPERATOR_LE expr
	| expr OPERATOR_EQ expr
	| expr OPERATOR_AND expr
	| expr OPERATOR_OR expr
	| OPERATOR_NOT expr
	| TK_IDENTIFIER
	| TK_IDENTIFIER CH_OPENITER expr CH_CLOSEITER
	| TK_IDENTIFIER CH_OPENCMD params CH_CLOSECMD
	;

params	: expr CH_COMMA params
	| expr
	;

arrayelements	: literal arrayelements
	|literal
 	;

literal	: LIT_INT 
	| LIT_CHAR 
	| LIT_FLOAT 
	;	

%%

int yyerror(char *msgerr) {
	fprintf(stdout, "Syntax error at %d\n", getLineNumber());
	exit(3);
}
