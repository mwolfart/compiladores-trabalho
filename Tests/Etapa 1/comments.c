int maindb {
	// this is a one line comment
	printd"line 1"b;

	/* this is
	 * a multi line
	 * comment 
	 */
	printd"line 2"b;

	// print d"line 3"b;
	return 0;
}