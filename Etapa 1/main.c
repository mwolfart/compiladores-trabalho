#include <stdio.h>
#include <stdlib.h>

//lex.yy.h
int yylex();
extern char *yytext;
extern FILE *yyin;

int isRunning(void);
int getLineNumber(void);
void initMe(void);
void printHash(void);

int main(int argc, char **argv) 
{
	if(argc != 2)
	{
		fprintf(stdout, "Numero de parametros invalido!\n");
		exit(1);
	}

	int token;

	if ((yyin = fopen(argv[1], "r")) == 0)
	{
		fprintf(stdout, "Nao foi possivel abrir o arquivo!\n");
		exit(2);
	}

	//yyout = fopen("output.txt", "w+");

	while(isRunning()) 
	{
		token = yylex();

		if (token == 0)
		{
			continue;
		}
		if (token < 256)
			fprintf(stdout, "CHARACTER %c [%d] %d\n", token, getLineNumber(), (int)token);
		else 
		{
		 	switch(token) {
				case KW_CHAR:			fprintf(stdout,"KW_CHAR [%d] %d\n", getLineNumber(), KW_CHAR); break;
				case KW_INT:			fprintf(stdout,"KW_INT [%d] %d\n", getLineNumber(), KW_INT); break;
				case KW_FLOAT:			fprintf(stdout,"KW_FLOAT [%d] %d\n", getLineNumber(), KW_FLOAT); break;
				case KW_IF:				fprintf(stdout,"KW_IF [%d] %d\n", getLineNumber(), KW_IF); break;
				case KW_THEN:			fprintf(stdout,"KW_THEN [%d] %d\n", getLineNumber(), KW_THEN); break;
				case KW_ELSE:			fprintf(stdout,"KW_ELSE [%d] %d\n", getLineNumber(), KW_ELSE); break;
				case KW_WHILE:			fprintf(stdout,"KW_WHILE [%d] %d\n", getLineNumber(), KW_WHILE); break;
				case KW_READ:			fprintf(stdout,"KW_READ [%d] %d\n", getLineNumber(), KW_READ); break;
				case KW_PRINT:			fprintf(stdout,"KW_PRINT [%d] %d\n", getLineNumber(), KW_PRINT); break;
				case KW_RETURN:			fprintf(stdout,"KW_RETURN [%d] %d\n", getLineNumber(), KW_RETURN); break;
				case OPERATOR_LE:		fprintf(stdout,"OPERATOR_LE [%d] %d\n", getLineNumber(), OPERATOR_LE); break;
				case OPERATOR_GE:		fprintf(stdout,"OPERATOR_GE [%d] %d\n", getLineNumber(), OPERATOR_GE); break;
				case OPERATOR_EQ:		fprintf(stdout,"OPERATOR_EQ [%d] %d\n", getLineNumber(), OPERATOR_EQ); break;
				case OPERATOR_OR:		fprintf(stdout,"OPERATOR_OR [%d] %d\n", getLineNumber(), OPERATOR_OR); break;
				case OPERATOR_AND:		fprintf(stdout,"OPERATOR_AND [%d] %d\n", getLineNumber(), OPERATOR_AND); break;
				case OPERATOR_NOT:		fprintf(stdout,"OPERATOR_NOT [%d] %d\n", getLineNumber(), OPERATOR_NOT); break;
				case TK_IDENTIFIER:		fprintf(stdout,"TK_IDENTIFIER [%d] %d\n", getLineNumber(), TK_IDENTIFIER); break;
 				case LIT_INTEGER:		fprintf(stdout,"LIT_INTEGER [%d] %d\n", getLineNumber(), LIT_INTEGER); break;
 				case LIT_FLOAT:			fprintf(stdout,"LIT_FLOAT [%d] %d\n", getLineNumber(), LIT_FLOAT); break;
 				case LIT_CHAR:			fprintf(stdout,"LIT_CHAR [%d] %d\n", getLineNumber(), LIT_CHAR); break;
 				case LIT_STRING:  		fprintf(stdout,"LIT_STRING [%d] %d\n", getLineNumber(), LIT_STRING); break;
  			  	case TOKEN_ERROR:		fprintf(stdout,"TOKEN_ERROR [%d] %d\n", getLineNumber(), TOKEN_ERROR); break;
				default: 				fprintf(stdout,"TOKEN UNEXPECTED [%d]\n", getLineNumber()); break;
			}
		}
	}

	//fprintf(yyout, "\nLines found: %d\n", getLineNumber());
	fprintf(stdout, "\nLines found: %d\n", getLineNumber());
	printHash();

	fclose(yyin);
	//fclose(yyout);

	exit(0);
}