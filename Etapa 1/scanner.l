%{
	#include "tokens.h"
	#include "hash.c"
	#include "main.c"

	int yyrunning = 1;

	int getLineNumber(void) {
		return yylineno;
	}

	int isRunning(void) {
		return yyrunning;
	}

	void initMe(void) {
		hashInit();
	}

	void printHash(void) {
		hashPrint();
	}
%}

%x COMMENT

%%

"\n"					yylineno++;

"//".*"\n"				yylineno++;

"char"					return KW_CHAR;
"int"					return KW_INT;
"float"					return KW_FLOAT;
"if"					return KW_IF;
"then"					return KW_THEN;
"else"					return KW_ELSE;
"while"					return KW_WHILE;
"read"					return KW_READ;
"print"					return KW_PRINT;
"return"				return KW_RETURN;

"<="					return OPERATOR_LE;
">="					return OPERATOR_GE;
"=="					return OPERATOR_EQ;
"or"					return OPERATOR_OR;
"and" 					return OPERATOR_AND;
"not" 					return OPERATOR_NOT;


[_A-Zace-or-z0-9]*[_A-Zace-or-z]+[_A-Zace-or-z0-9]*				{  hashFindElseInsert(TK_IDENTIFIER, yytext);	return TK_IDENTIFIER;  }

\'.?\'					{  hashFindElseInsert(LIT_CHAR, yytext)     ; return LIT_CHAR;       }
[0-9]+					{  hashFindElseInsert(LIT_INTEGER, yytext)  ; return LIT_INTEGER;    }
[0-9]*\.[0-9]+			{  hashFindElseInsert(LIT_FLOAT, yytext)     ; return LIT_FLOAT;       }
\"(\\.|[^"\\])*\"		{  hashFindElseInsert(LIT_STRING, yytext)   ; return LIT_STRING;     }


"/*"    {BEGIN(COMMENT);}

<COMMENT>"*/" {BEGIN(INITIAL);}
<COMMENT>"\n" {yylineno++;}
<COMMENT>.   

[,;:dbqp{}\+\-\*/<>=!\$&#]	return yytext[0];
[ \t\r]							 

.							return TOKEN_ERROR;

%%

int yywrap(void) {
	yyrunning = 0;
	return 1;
}