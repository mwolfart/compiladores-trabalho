#include "hash.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

HASH_NODE* table[HASH_SIZE];

void hashInit()
{
    for (int i = 0; i <= HASH_SIZE; i++)
    {
        table[i] = 0;
    }
}

int hashAddress(char *text)
{
    int address = 1;
    for (int i = 0; i < strlen(text); i++)
        address = (address * text[i]) % HASH_SIZE + 1;
    return address - 1;
}

HASH_NODE* hashFindElseInsert(int type, char *text) 
{
    HASH_NODE* foundNode = hashFind(text);

    if (foundNode)
        return foundNode;
    else
        return hashInsert(type, text);
}

HASH_NODE* hashInsert(int type, char *text)
{
    int nodeAddress = hashAddress(text);
    HASH_NODE* newNode = (HASH_NODE *)calloc(1, sizeof(HASH_NODE));

    newNode->type = type;
    newNode->text = calloc(strlen(text) + 1, sizeof(char));
    strcpy(newNode->text, text);
    newNode->next = table[nodeAddress];
    table[nodeAddress] = newNode;

    return newNode;
}

HASH_NODE* hashFind(char *text)
{
    int address = hashAddress(text);
    HASH_NODE* nodeAux;

    for (nodeAux = table[address]; nodeAux != NULL; nodeAux = nodeAux->next)
        if (!strcmp(text, nodeAux->text))
            return nodeAux;

    return NULL;
}

void hashPrint(void)
{
    HASH_NODE* nodeAux;

    for (int i = 0; i < HASH_SIZE; i++)
        for (nodeAux = table[i]; nodeAux; nodeAux = nodeAux->next)
            fprintf(stdout, "Tabela[%d] possui %s\n", i, nodeAux->text);
}